import matplotlib.pyplot as plt
import numpy as np
import Signal
from typing import List
from matplotlib.ticker import MultipleLocator


def bpsk_success_rate(binary_input: List[float], frequency: int, sample_freq: int,
                      number_of_tests: int, noise: float):

    success_count = 0
    for i in range(0, number_of_tests):

        signal_input = Signal.modulate_bpsk(binary_input, frequency, sample_freq)
        signal_with_noise = Signal.add_noise(signal_input, noise)
        binary_output = Signal.demodulate_bpsk(signal_with_noise, frequency, sample_freq)

        if binary_input == binary_output:
            success_count += 1

    success_rate = (success_count/number_of_tests) * 100

    return success_rate


def qpsk_success_rate(binary_input: List[float], frequency: int, sample_freq: int,
                      number_of_tests: int, noise: float):

    success_count = 0
    for i in range(0, number_of_tests):

        signal_input = Signal.modulate_qpsk(binary_input, frequency, sample_freq)
        signal_with_noise = Signal.add_noise(signal_input, noise)
        binary_output = Signal.demodulate_qpsk(signal_with_noise, frequency, sample_freq)

        if binary_input == binary_output:
            success_count += 1

    success_rate = (success_count/number_of_tests) * 100

    return success_rate

def test_bpsk_with_plots(binary_input: List[float], frequency: int, sample_freq: int, noise: float):

    signal_input = Signal.modulate_bpsk(binary_input, frequency, sample_freq)
    signal_with_noise = Signal.add_noise(signal_input, noise)
    binary_output = Signal.demodulate_bpsk(signal_with_noise, frequency, sample_freq)

    samples_quantity = (sample_freq / frequency) * len(binary_input)
    x = np.arange(samples_quantity)

    text_upper = "Nadano " + binary_input.__str__()
    if binary_input == binary_output:
        text_bottom = "Sygnał został odebrany bez błędów"
    else:
        text_bottom = "Sygnał odebrany NIE jest równy nadanemu \n Odebrano " + binary_output.__str__()

    period = sample_freq // frequency
    spacing = period
    minor_locator = MultipleLocator(spacing)

    fig = plt.figure(1)
    subplot_upper = fig.add_subplot(211)
    subplot_upper.plot(x, signal_input)
    subplot_upper.title.set_text(text_upper)
    subplot_upper.xaxis.set_minor_locator(minor_locator)
    subplot_upper.axhline(y=0, color='r')
    subplot_upper.grid(which='minor')

    subplot_bottom = fig.add_subplot(212)
    subplot_bottom.plot(x, signal_with_noise)
    subplot_bottom.title.set_text(text_bottom)
    subplot_bottom.axhline(y=0, color='r')
    subplot_bottom.xaxis.set_minor_locator(minor_locator)
    subplot_bottom.grid(which='minor')

    plt.show()


def test_qpsk_with_plots(binary_input: List[float], frequency: int, sample_freq: int, noise: float):

    signal_input = Signal.modulate_qpsk(binary_input, frequency, sample_freq)
    signal_with_noise = Signal.add_noise(signal_input, noise)
    binary_output = Signal.demodulate_qpsk(signal_with_noise, frequency, sample_freq)

    samples_quantity = (sample_freq / frequency) * len(binary_input) / 2
    x = np.arange(samples_quantity)

    text_upper = "Nadano " + binary_input.__str__()
    if binary_input == binary_output:
        text_bottom = "Sygnał został odebrany bez błędów"
    else:
        text_bottom = "Sygnał odebrany NIE jest równy nadanemu \n Odebrano " + binary_output.__str__()

    period = sample_freq // frequency
    spacing = period
    minor_locator = MultipleLocator(spacing)

    fig = plt.figure(1)
    subplot_upper = fig.add_subplot(211)
    subplot_upper.plot(x, signal_input)
    subplot_upper.title.set_text(text_upper)
    subplot_upper.xaxis.set_minor_locator(minor_locator)
    subplot_upper.axhline(y=0, color='r')
    subplot_upper.grid(which='minor')

    subplot_bottom = fig.add_subplot(212)
    subplot_bottom.plot(x, signal_with_noise)
    subplot_bottom.title.set_text(text_bottom)
    subplot_bottom.axhline(y=0, color='r')
    subplot_bottom.xaxis.set_minor_locator(minor_locator)
    subplot_bottom.grid(which='minor')

    plt.show()