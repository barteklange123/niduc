import ImageLoader
import Signal
import Tests
from tkinter import *
import random as rnd
from typing import List

class Window(Frame):

    binary_input = [0, 0, 1, 0, 0, 1, 1, 0, 1, 0, 1, 0]
    frequency = 200
    sample_freq = 6000

    def __init__(self, master = None):
        Frame.__init__(self, master)

        self.master = master

        self.init_window()

    def init_window(self):

        self.master.title("GUI")
        self.pack(fill=BOTH, expand=1)

        self.bpskPltButton = Button(self, text="plot BPSK", command=self.plotBpsk)
        self.bpskPltButton.place(x=0, y=200)

        self.qpskPltButton = Button(self, text="plot QPSK", command=self.plotQpsk)
        self.qpskPltButton.place(x=0, y=250)

        self.bpskSuccButton = Button(self, text="wskaźnik sukcesu BPSK", command=self.succBpsk)
        self.bpskSuccButton.place(x=70, y=200)

        self.qpskSuccButton = Button(self, text="wskaźnik sukcesu QPSK", command=self.succQpsk)
        self.qpskSuccButton.place(x=70, y=250)

        confirmButton = Button(self, text="Zaakceptuj", command=self.confVal)
        confirmButton.place(x=20, y=120)

        freqBut = Button(self, text="Zakończ", command=self.quiting)
        freqBut.place(x=340, y=250)


        self.freqEntry = Entry(self)
        self.freqEntry.place(x=140, y=0)
        self.freqEntry.insert(0, string="200")

        self.sampFreqEntry = Entry(self)
        self.sampFreqEntry.place(x=140, y=42)
        self.sampFreqEntry.insert(0, string="6000")

        self.binIptEntry = Entry(self)
        self.binIptEntry.place(x=140, y=80)
        self.binIptEntry.insert(0, string="001001101010")

        freqLabel = Label(self, text="częstotliwość", width=15, justify=RIGHT)
        freqLabel.place(x=0, y=0)

        sampFreqLabel = Label(self, text="częstotliwość\npróbkowania", width=15, justify=RIGHT)
        sampFreqLabel.place(x=0, y=35)

        binIptLabel = Label(self, text="ciąg bitów", width=15, justify=RIGHT)
        binIptLabel.place(x=0, y=80)

        self.errLabel = Label(self, text="")
        self.errLabel.place(x=100, y=113)


    def quiting(self):

        self.quit()

    def confVal(self):

        self.frequency = int(self.freqEntry.get())
        self.sample_freq = int(self.sampFreqEntry.get())

        a = self.binIptEntry.get()
        b = []

        for x in range(len(a)):
            b.append(int(a[x]))
        self.binary_input = b
        therest = self.sample_freq % self.frequency
        corlen = (len(a)) % 4

        if therest != 0:
            self.errLabel.config(text='częstotliwość próbkowania musi \nbyć wielokrotnością częstotliwości', fg="red")
            self.bpskPltButton.config(state="disabled")
            self.qpskPltButton.config(state="disabled")
            self.bpskSuccButton.config(state="disabled")
            self.qpskSuccButton.config(state="disabled")

        elif corlen != 0:
            self.errLabel.config(text='długość ciągu bitów musi być\n wielokrotnością cyfry 4', fg="red")
            self.bpskPltButton.config(state="disabled")
            self.qpskPltButton.config(state="disabled")
            self.bpskSuccButton.config(state="disabled")
            self.qpskSuccButton.config(state="disabled")

        elif therest == 0 and corlen == 0:
            self.errLabel.config(text='')
            self.bpskPltButton.config(state="active")
            self.qpskPltButton.config(state="active")
            self.bpskSuccButton.config(state="active")
            self.qpskSuccButton.config(state="active")




    def plotBpsk(self):

        Tests.test_bpsk_with_plots(self.binary_input, self.frequency, self.sample_freq, 1.1)

    def plotQpsk(self):

        Tests.test_qpsk_with_plots(self.binary_input, self.frequency, self.sample_freq, 2.1)

    def succBpsk(self):

        success_rate = 100.0
        noise = 1.0
        noise_tick = 0.1
        while success_rate == 100.0:
            success_rate = Tests.bpsk_success_rate(self.binary_input, self.frequency, self.sample_freq, 1000, noise)
            # print(noise)
            noise += noise_tick

        print("Pierwsze błędy przy BPSK zaczęły się pojawiać przy szumie " + noise.__str__())

    def succQpsk(self):

        success_rate = 100.0
        noise = 1.0
        noise_tick = 0.1
        while success_rate == 100.0:
            success_rate = Tests.qpsk_success_rate(self.binary_input, self.frequency, self.sample_freq, 1000, noise)
            # print(noise)
            noise += noise_tick

        print("Pierwsze błędy przy QPSK zaczęły się pojawiać przy szumie " + noise.__str__())

def main():
    # binary = ImageLoader.img2binary()

    root = Tk()
    root.geometry("400x300")

    app = Window(root)

    root.mainloop()
"""

    frequency = 200
    sample_freq = 6000
    binary_input = generate_random_binary_array(16)
    print(len(binary_input))

    print(Tests.bpsk_success_rate(binary_input, frequency, sample_freq, 1000, 2.5))
    Signal.show_plot(Signal.modulate_16qam(binary_input, frequency, sample_freq), frequency, sample_freq,
                     len(binary_input), bit_block_size=4)

    Tests.test_bpsk_with_plots(binary_input, frequency, sample_freq, 1.1)
    Tests.test_qpsk_with_plots(binary_input, frequency, sample_freq, 1.1)

    success_rate = 100.0
    noise = 1.0
    noise_tick = 0.1
    while success_rate == 100.0:
        success_rate = Tests.bpsk_success_rate(binary_input, frequency, sample_freq, 1000, noise)
        # print(noise)
        noise += noise_tick

    print("Pierwsze błędy przy BPSK zaczęły się pojawiać przy szumie " + noise.__str__())

    success_rate = 100.0
    noise = 1.0
    noise_tick = 0.1
    while success_rate == 100.0:
        success_rate = Tests.qpsk_success_rate(binary_input, frequency, sample_freq, 1000, noise)
        # print(noise)
        noise += noise_tick

    print("Pierwsze błędy przy QPSK zaczęły się pojawiać przy szumie " + noise.__str__())


def generate_random_binary_array(array_length):
    binary_array: List[int] = []

    for i in range(0, array_length):
        binary_array.append(rnd.choice([0, 1]))

    return binary_array
"""

main()
